package com.statepattern.model.action.mainstate;

import com.statepattern.model.action.mainstate.helper.LoggerMessages;
import com.statepattern.model.action.maincontext.MainContext;

public interface MainState {
    default void addTask(MainContext mainContext) {
        new LoggerMessages().cantDoThisMethod("addTask");
    }

    default void addUser(MainContext mainContext) {
        new LoggerMessages().cantDoThisMethod("addUser");
    }

    default void addTaskToUser(MainContext mainContext) {
        new LoggerMessages().cantDoThisMethod("addTaskToUser");
    }

    default void startSprint(MainContext mainContext) {
        new LoggerMessages().cantDoThisMethod("startSprint");
    }

    default void endSprint(MainContext mainContext) {
        new LoggerMessages().cantDoThisMethod("endSprint");
    }
}
