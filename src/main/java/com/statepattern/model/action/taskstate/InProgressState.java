package com.statepattern.model.action.taskstate;


import com.statepattern.model.action.taskcontext.TaskContext;

public class InProgressState implements TaskState {

    @Override
    public void toReview(TaskContext context) {
        context.setTaskState(new ReviewState());
        LOGGER.info("base.Task is reviewing.");
    }

    @Override
    public void toBlock(TaskContext context) {
        context.setTaskState(new BlockState());
        LOGGER.info("base.Task is blocked.");
    }
}
