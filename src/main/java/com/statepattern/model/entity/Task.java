package com.statepattern.model.entity;

import java.util.Objects;

public class Task {
    private int id;
    private String toDo;
    private boolean isUsed;

    public Task() {
        this.isUsed = false;
    }

    public Task(int id, String toDo) {
        this.id = id;
        this.toDo = toDo;
        this.isUsed = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToDo() {
        return toDo;
    }

    public void setToDo(String toDo) {
        this.toDo = toDo;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        this.isUsed = used;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                Objects.equals(toDo, task.toDo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, toDo);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", toDo='" + toDo + '\'' +
                '}';
    }
}
