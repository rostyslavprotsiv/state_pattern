package com.statepattern.view;

import com.statepattern.controller.Controller;

public final class Menu extends AbstractMenu {

    public Menu() {
        menu.put(0, "Add task");
        menu.put(1, "Add user");
        menu.put(2, "Add task to user");
        menu.put(3, "Start Sprint and show result");
        menu.put(4, "Exit");
        methodsForMenu.put(0, this::addTask);
        methodsForMenu.put(1, this::addUser);
        methodsForMenu.put(2, this::addTaskToUser);
        methodsForMenu.put(3, this::showExecution);
        methodsForMenu.put(4, this::quit);
    }

    public void show() {
        out();
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for state_Pattern");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void addUser() {
        CONTROLLER.addUser();
        LOGGER.info("User is added");
    }

    private void addTask() {
        CONTROLLER.addTask();
        LOGGER.info("Task is added");
    }

    private void addTaskToUser() {
        CONTROLLER.addTaskToUser();
        LOGGER.info("Tasks is added to Users");
    }

    private void showExecution() {
        LOGGER.info(CONTROLLER.executeAndGet());
        CONTROLLER = new Controller();
    }
}
