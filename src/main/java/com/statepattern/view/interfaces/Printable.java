package com.statepattern.view.interfaces;

@FunctionalInterface
public interface Printable {
    void print();
}
