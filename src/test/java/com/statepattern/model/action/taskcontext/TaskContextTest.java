package com.statepattern.model.action.taskcontext;

import com.statepattern.model.entity.Task;
import com.statepattern.model.entity.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class TaskContextTest {

    @Test
    public void testToStart() {
        List<Task> tasks = new ArrayList<>();

        tasks.add(new Task(1," toDO"));
        tasks.add(new Task(2," toDO"));
        TaskContext context = new TaskContext(new User(1, "kfkffk",
                tasks, new ArrayList<>(), new ArrayList<>()));
        context.toBlock();
        context.toStart();
        assertFalse(context.getUser().getBlockedTasks().isEmpty());
    }
}
